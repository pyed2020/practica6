import java.util.Queue;

public class Cola {
    private Object [] array;
    private Object ultimo;
    private int numObjetos;
    
    private final int TAMAÑO_INICIAL = 10;

    public static void main(String[] args) {
        
    }
    
    public Cola (){
        this.array = new Object [TAMAÑO_INICIAL];
        this.numObjetos = 0;
        ultimo = array[numObjetos];
    }

    public boolean add(Object obj){
       
        this.actualizarTamañoArray();
        this.avanzarUnaPosicion();
        
        array[0] = obj;
        numObjetos++;
        ultimo = array [numObjetos];
        
    }

    public Object element(){
        return ultimo;
    }

    public Object peek (){
        return ultimo;
    }

    public Object poll () {
        Object o = ultimo.clone();
        ultimo = array[numObjetos-1];
        array[numObjetos] = null;
        return o;
    }

    public Object remove (){
        ultimo = array[numObjetos-1];
        Object o = array [numObjetos].clone();
        array[numObjetos] = null;
        return o;
    }

    public void actualizarTamañoArray (){
        Object [] nuevoArray = new Object [array.length*2];
        if (numObjetos >= array.length/2){
            for (int i = 0; i < array.length; i++){
                nuevoArray[i] = array[i];
            } 
        }
        this.array = nuevoArray;
    }

    public void avanzarUnaPosicion (){
        Object [] nuevoArray = new Object [array.length];
        for (int i = 0; i < array.length; i++){
            nuevoArray[i+1] = array[i];
        }
        this.array = nuevoArray;
    }
}
