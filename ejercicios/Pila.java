public class Pila {
    private Object top;
    private Object [] array;
    private int numObjetos;

    private final int TAMAÑO_INICIAL = 10;

    public static void main(String[] args) {
        
    }

    public Pila (){
        this.array = new Object [TAMAÑO_INICIAL];
        this.numObjetos = 0;
        top = array [0];
    }

    public Object push ( Object obj){
        this.actualizarArray();
        array [numObjetos] = obj;
        top = array[numObjetos];
        numObjetos++;
        return obj;
    }

    public Object pop (){
        assert numObjetos >=1;
        Object obj = top.clone();

        array [numObjetos-1] = null;
        if (numObjetos == 1){
            top = null;
        } else{
            top = array [numObjetos-2];
        }
        numObjetos--;
        return obj;
    }

    public boolean empty (){
        for (Object obj : array ){
            if (obj != null) 
                return false;
        }
        return true;
    }

    public Object peek (){
        return top;
    }

    public int search (Object obj){
        for (int i = numObjetos; i > 0; i--){
            if (array[i].equals(obj)){
                return numObjetos - i;
            }
        }
        return -1;
    }

    

    private void actualizarArray () {
        Object [] nuevoArray;
        if (numObjetos >= array.length/2){
            nuevoArray = new Object [array.length*2];
            for (int i = 0; i < numObjetos; i++){
                nuevoArray [i] = array [i];
            }
            array = nuevoArray;
        } 
    }
}
