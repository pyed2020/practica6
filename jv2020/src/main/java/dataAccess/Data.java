package dataAccess;

import java.util.ArrayList;

import models.Mail;
import models.Nif;
import models.Password;
import models.Session;
import models.Simulation;
import models.User;
import models.User.RoleUser;
import utils.EasyDate;
//TODO: Repasar el método sortUsersData
//En esta versión, los métodos sort no están implementados
public class Data {

	private ArrayList<User> usersData;
	private ArrayList<Session> sessionsData;
	private ArrayList<Simulation> simulationsData;
	// private int registeredUser;
	// private int registeredSessions;
	// private int registeredSimulations;

	public Data() {
		this.usersData = new ArrayList<User>();
		this.sessionsData = new ArrayList<Session>();
		this.simulationsData = new ArrayList<Simulation>();
		// this.registeredUser = 0;
		// this.registeredSessions = 0;
		// this.registeredSessions = 0;
		loadIntegratedUsers();
	}

	private void loadIntegratedUsers() {
		this.createUser(new User(new Nif("00000000T"), "Admin", "Admin Admin", "La Iglesia, 0, 30012, Patiño",
				new Mail("admin@gmail.com"), new EasyDate(2000, 1, 14), new EasyDate(2021, 1, 14),
				new Password("Miau#00"), RoleUser.REGISTERED));
		this.createUser(new User(new Nif("00000001R"), "Guest", "Guest Guest", "La Iglesia, 0, 30012, Patiño",
				new Mail("guest@gmail.com"), new EasyDate(2000, 1, 14), new EasyDate(2021, 1, 14),
				new Password("Miau#00"), RoleUser.REGISTERED));
	}

	// Users

	public User findUser(String id) {
		int pos = indexOfUser(id);
		if (pos == -1) {
			return null;
		}
		return usersData.get(pos);
	}

	private int indexOfUser(String id) {
		// Búsqueda binaria del usuario a partir del id
		// Hay que tener a los usuarios ordenados para que sirva este algoritmo de
		// búsqueda
		int start = 0;
		int end = usersData.size();
		int pos;
		while (start >= end) {
			pos = (start + end) / 2;
			if (usersData.get(pos).getId().equals(id)) {
				return pos;
			} else if (usersData.get(pos).getId().compareTo(id) < 0) {
				start = pos + 1;
			} else {
				start = pos - 1;
			}
		}
		return -1;
	}

	public void createUser(User user) {
		if (findUser(user.getNif().getText()) == null) {
			this.usersData.add(user);
			// this.registeredUser++;
			return;
		}
	}

	public void sortUsersData(User user) {
		if (usersData.size() > 0) {
			int pos = usersData.size() - 1;
			if (usersData.get(pos).getId().compareTo(user.getId()) < 0) {
				usersData.add(pos, user);
				return;
			}
		}
		usersData.add(user);
	}

	public void updateUser(User user) {
		User userOld = findUser(user.getNif().getText());
		if (userOld != null) {
			this.usersData.add(indexOfUser(userOld.getId()), user);
			return;
		}
	}

	public void deleteUser(String id) {
		User user = findUser(id);
		if (user != null) {
			this.usersData.remove(indexOfUser(user.getId()));
			// this.registeredUser--;
			return;
		}
	}

	// Sessions

	public Session findSession(String id) {
		int pos = indexOfSession(id);
		if (pos == -1) {
			return null;
		}
		return sessionsData.get(pos);
	}

	public int indexOfSession(String id) {
		int start = 0;
		int end = sessionsData.size();
		int pos;
		while (start >= end) {
			pos = (start + end) / 2;
			if (sessionsData.get(pos).getId().equals(id)) {
				return pos;
			} else if (sessionsData.get(pos).getId().compareTo(id) < 0) {
				start = pos + 1;
			} else {
				start = pos - 1;
			}
		}
		return -1;
	}

	public void createSession(Session session) {
		this.sessionsData.add(session);
		// this.registeredSessions++;
		return;
	}

	public void sortSessionsData(Session session) {
		if (sessionsData.size() > 0) {
			int pos = sessionsData.size() - 1;
			if (sessionsData.get(pos).getId().compareTo(session.getId()) < 0) {
				sessionsData.add(pos, session);
				return;
			}
		}
		sessionsData.add(session);
	}

	public void updateSession(Session session) {
		Session sessionOld = this.findSession(session.getId());
		if (sessionOld != null) {
			this.sessionsData.add(indexOfSession(sessionOld), session);
			return;
		}
	}

	private int indexOfSession(Session session) {
		for (int i = 0; i < this.sessionsData.size(); i++) {
			if (session.equals(this.sessionsData.get(i))) {
				return i;
			}
		}
		return -1;
	}

	// Simulations

	public Simulation findSimulation(String id) {
		int pos = indexOfSimulation(id);
		if (pos == -1) {
			return null;
		}
		return simulationsData.get(pos);
	}

	public int indexOfSimulation(String id) {
		int start = 0;
		int end = simulationsData.size();
		int pos;
		while (start >= end) {
			pos = (start + end) / 2;
			if (simulationsData.get(pos).getId().equals(id)) {
				return pos;
			} else if (simulationsData.get(pos).getId().compareTo(id) < 0) {
				start = pos + 1;
			} else {
				start = pos - 1;
			}
		}
		return -1;
	}

	public void createSimulation(Simulation simulation) {
		if (findUser(simulation.getId()) == null) {
			this.simulationsData.add(simulation);
			// this.registeredSimulations++;
			return;
		}
	}

	public void sortSimulationsData(Simulation simulation) {
		if (simulationsData.size() > 0) {
			int pos = simulationsData.size() - 1;
			if (simulationsData.get(pos).getId().compareTo(simulation.getId()) < 0) {
				simulationsData.add(pos, simulation);
				return;
			}
		}
		simulationsData.add(simulation);
	}

	public void updateSimulation(Simulation simulation) {
		Simulation simulationOld = this.findSimulation(simulation.getId());
		if (simulationOld != null) {
			this.simulationsData.add(indexOfSimulation(simulationOld.getId()), simulation);
			return;
		}
	}


	// Intento de generalizar

	public void sortSomethingData(Object obj) {

	}

	/* public int indexOfSomething(String id, Object obj) {
		// Intento de indexOf genérico
		// Se puede hacer haciendo una clase padre de User Session y Simulation que
		// contenga el método getId()
		ArrayList<?> arr;
		if (obj.getClass().getName().equals("User")) {
			arr = usersData;
		} else if (obj.getClass().getName().equals("Session")) {
			arr = sessionsData;
		} else {
			arr = simulationsData;
		}
		int start = 0;
		int end = arr.size();
		int pos;
		while (start >= end) {
			pos = (start + end) / 2;
			if (arr.get(pos).getId().equals(id)) {
				return pos;
			} else if (arr.get(pos).getId().compareTo(id) < 0) {
				start = pos + 1;
			} else {
				start = pos - 1;
			}
		}
		return -1;
	}  */
}
