package models;

public class ModelsException extends Exception {
    public ModelsException (String message) {
        super(message);
    }
}
