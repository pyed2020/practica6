package models;

public class Position {
    private int x;
    private int y;

    public Position (int x, int y){
        this.setX(x);
        this.setY(y);
    }

    public Position (){
    }

    public Position (Position position){
        assert position != null;
        this.x = position.x;
        this.y = position.y;
    }

    public void setX (int x){
        assert x >= 0;
        this.x = x;
    }

    public void setY (int y){
        assert y >= 0;
        this.y = y;
    }
}
