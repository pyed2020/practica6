package models;

import utils.EasyDate;

public class Simulation {

	public enum StateSimulation {PREPARED, RUNNING, FINISHED};

	private User user;
	private EasyDate date;
	private World world;
	private StateSimulation state;

	private static final int SIMULATION_CYCLES = 20;

	public Simulation(User user, EasyDate date, World world) {
		this.setUser(user);
		this.setDate(date);
		this.setWorld(world);
		this.getWorld().setName(user.getName());
		this.state = StateSimulation.PREPARED;
	}

	public Simulation() {
		this(new User(), 
				EasyDate.now(),
				new World()
				);
	}

	public Simulation(Simulation simulation) {
		assert simulation != null;
		
		this.user = simulation.user;
		this.date =	EasyDate.today();
		this.world = simulation.world;
		this.state = simulation.state;
	}

	public World getWorld (){
		return this.world;
	}

	public String getId() { 
		return this.user.getId() + ":" + this.date.toStringTimeStamp();
	}
	
	public User getUser() {
		return this.user;
	}

	public EasyDate getDate() {
		return this.date;
	}

	public StateSimulation getState() {
		return this.state;
	}

	public void setUser(User user) {
		assert user != null;
		this.user = user;
	}

	public void setDate(EasyDate date) {
		assert date != null;	
		if (isValidDate(date)) {
			this.date = date;
		}
	}

	private boolean isValidDate(EasyDate date) {	
		return !date.isAfter(EasyDate.now());
	}

	public void setWorld(World world) {
		assert world != null;
		this.world = world;
	}

	@Override
	public String toString() {
		return String.format(			
				"%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n",
						"user:", this.user.getName(), 
						"date:", this.date, 
						"gridType:", this.world.getGridType(), 
						"state:", this.state
				);
	}

	@Override
	public Simulation clone() {
		return new Simulation(this);
	}

	public void runDemo() {
		this.loadDemoGrid();
		int generation = 0; 
		do {
			System.out.println("\nGeneración: " + generation);
			this.showWorld();
			this.updateWorld();
			generation++;
		}
		while (generation < Simulation.SIMULATION_CYCLES);
	}

	/**
	 * Carga datos demo en la matriz que representa el mundo. 
	 */
	private void loadDemoGrid() {
		this.world.loadDemoGrid();
	}

	/**
	 * Despliega en la consola el estado almacenado, corresponde
	 * a una generación del Juego de la vida.
	 */
	public void showWorld() {
		this.world.showGrid();
	}

	public void updateWorld (){
		this.world.updateWorld();
	}

} 
