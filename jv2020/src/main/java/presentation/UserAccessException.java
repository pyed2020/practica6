package presentation;

public class UserAccessException extends Exception {
    public UserAccessException (String message) {
        super(message);
    }
}
